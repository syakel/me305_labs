# -*- coding: utf-8 -*-
'''@file                    main.py
   @brief                   Cycles an LED through 3 lighting patterns
   @details                 This program embodies a finite state machine containing 5 states: init, waiting for
                            user input, and three distinct LED lighting patterns: square wave, sawtooth wave, and
                            sine wave. Transitions between the states are activated by the user pressing the B1
                            button on the Nucleo.
                            
                            The main file allows the task_user and task_encoder files to share variables with each
                            other. The diagram is as follows:
                                
                            \image html taskdiagram.PNG "Task Interactions"
                            
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 5, 2021
'''

import time

import utime

import pyb

import math

## @brief The button variable is what is momentarily set as true each time the button is pressed and set to false immediately after. This variable allows the user to trigger state transitions.
#  @details Set to true when button pressed, and set to back false immediately before state transition.
#

button = False

## @brief current_time is the variable which allows the user to create the constantly changing waveform patterns.
#  @details Is updated continuously by the update_timer function
#
current_time = 0

def onButtonPressFCN(IRQ_src):
    '''@brief           Defines button to be "true" when pressed by user
       @details         This function changes the global variable "button" to be true when the B1
                        button on the Nucleo is pressed by the user. When button=True, the program transitions
                        into the next appropriate state.
    '''
    
    global button 
    
    button = True
    
def update_stw(current_time):
    '''@brief           This function takes the current time and calculates
                        the corresponding sawtooth wave which the LED will 
                        display.
       @details         The sawtooth wave value is calculated using a modulus
                        which returns a brightness percentage.
       @return          This function returns a value which indicates a 
                        brightness percentage that the LED will display.
    '''
    
    return 100*(current_time % 1.0)

def update_sqw(current_time):
    '''@brief           This function takes the current time and calculates
                        the corresponding square wave value which the LED will
                        display.
       @details         The square wave value is calculated using a modulus
                        which returns a true value if less than 0.5 and a 
                        false value if greater than 0.5.
                        
       @return          This function returns a brightness percentage of 100 
                        if the calculation is true and a percentage of 0 if
                        false.
    '''
    
    return 100*(current_time % 1.0 < 0.5)

def update_sin(current_time):
    '''@brief           This function takes the current time and calculates
                        the corresponding sin wave value which the LED will 
                        display.
       @details         The sin wave value is calculated using the Math 
                        library and equated to a percentage to be passed to
                        control the LED brightness.
       @return          Returns the sin wave value transposed up the Y-axis.
    '''
    
    return 100*(0.5*(math.sin(math.pi*current_time/5))+0.5)

def update_timer(start_time):
    '''@brief           This funciton updates the current time so that the LED
                        patterns can correctly be triggered.
       @details         This funciton uses a time difference between the last 
                        time iteration and the new time value to calculate the
                        current time.
       @return          This function returns the correct current time to the 
                        function.
    '''
    
    stop_time = utime.ticks_ms()
    
    current_time = utime.ticks_diff(stop_time, start_time) / 1000
    
    return current_time



if __name__ == '__main__':

    _state = 0

    _runs = 0
    
    _pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    
    _tim2 = pyb.Timer(2, freq = 20000)
    
    _t2ch1 = _tim2.channel(1, pyb.Timer.PWM, pin=_pinA5)
    
    _pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    
    _ButtonInt = pyb.ExtInt (_pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)


    
    ## The current state for this iteration of the FSM
    while (True):
        
        # Attempt to frun FSM unless Ctrl-c is hit
        try:
            if (_state==0): # Initialization state
                # Run the state 0 code
                print('')
                print("Welcome - this program blinks the Nucleo's LED in three distinct patterns: ")
                print('square wave, sawtooth wave, and sine wave. Press the B1 button on the Nucleo')
                print('to begin the program and to transition between each of the patterns.')
                _state = 1 # Transition to state 1
                print('')
                print('Waiting for user input...')
            
            elif(_state==1): # Waiting for input 
                # Run the state 1 code

                if button == True:
                    button = False
                    _state = 2
                    start_time = utime.ticks_ms()
                    print('Square wave selected')
                
            elif(_state==2): # Square wave
                # Run the state 2 code
                
                if button==False:
                    
                    current_time = update_timer(start_time)
                    
                    _square = update_sqw(current_time)
                    
                    _t2ch1.pulse_width_percent(_square)
                    
                    
                    pass
                
                else:
                    button = False
                    
                    ## @brief This variable is reset as each state transition is occuring, it allows the waveforms to start back at a zero reference point.
                    #  @details The variable is reset and used to calculate the current time in the update_timer function
                    #
                    
                    start_time = utime.ticks_ms()
                    _state = 3 # ?Transition to state 1?
                    print('Sawtooth wave selected')
                    
            elif(_state==3): # Saw tooth wave
                # Run the state 3 code
                
                if button==False:
                    
                    current_time = update_timer(start_time)
                    
                    _saw = update_stw(current_time)
                    
                    _t2ch1.pulse_width_percent(_saw)
                    
                    
                    pass
                else:
                    button = False
                    start_time = utime.ticks_ms()
                    _state = 4 # ?Transition to state 1?
                    print('Sin wave selected')
                    
            elif(_state==4): # Sin wave
                # Run the state 4 code
                
                if button==False:
                    
                    current_time = update_timer(start_time)
                    
                    _sin = update_sin(current_time)
                    
                    _t2ch1.pulse_width_percent(_sin)
                    
                    
                    pass
                else:
                    button = False
                    start_time = utime.ticks_ms()
                    _state = 2
                    print('Square wave selected')
            
            _runs += 1
            time.sleep(.01) # slow down the FSM iterations
            
        # Look for Ctrl-C which triggers program interrupt    
        except KeyboardInterrupt:
            break
        
    print('Program terminating')