''' @file                   motor_driver.py
    @brief                  Driver class for the motors
    @details                Allows for creation of motor objects to be
                            controlled by the control task.
                            
    @author                 Seth Yakel
    @author                 Nicole Croft
    @date                   8 December 2021
'''
import pyb

class Motor_Driver:
    ''' @brief
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or more objects of the Motor class
                    which can be used to perform motor control
    '''

    def __init__(self):
        ''' @brief          Initializes and returns a DRV8847 object
            @details
        '''
        ## PARALLELS THE TIMER FUNCTION
        
        self.tim3 = pyb.Timer(3, freq = 20000)
        
        pass

    def motor(self, PinA, PinB, chA, chB):
        ''' @brief          Initializes and returns a motor object associated with the DRV8847
            
            @return        An object of class Motor
        '''
        
        
        return Motor(PinA, PinB, chA, chB, self.tim3)


class Motor:
    ''' @brief              A motor class for one channel of the DRV8847
        @details            Objects of this class can be used to apply PWM to a given DC motor
    '''

    def __init__(self, PinA, PinB, chA, chB, tim3):
        ''' @brief          Initializes and returns a motor object associated with the DRV8847
            @details        Objects of this class should not be instantiated directly. Instead, create
                            a DRB8847 object and use that to create Motor objects using the method
                            DRV8847.motor()
        '''
        
        INA = pyb.Pin(PinA, pyb.Pin.OUT_PP)
        INB = pyb.Pin(PinB, pyb.Pin.OUT_PP)
        
        tim3 = tim3
        
        self._t3chA = tim3.channel(chA, tim3.PWM, pin=INA)
        self._t3chB = tim3.channel(chB, tim3.PWM, pin=INB)
        
        
        
        pass

    def set_duty(self, duty):
        ''' @brief          Set the PWM duty cycle for the motor channel
            @details        This method sets the duty cycle to be sent to the motor of the given level.
                            Positive values cause effort in one direction, negative values in the
                            opposite direction
            @param duty     A siged number holding the duty cycle of the pWM signal sent to the motor
        '''
        
        
        if duty>=0:
            
            self._t3chA.pulse_width_percent(100)
            self._t3chB.pulse_width_percent(100-duty)  #This must be a subtraction because it is a positive value
            
            
            
        if duty<0:
            
            self._t3chA.pulse_width_percent(100+duty)   #This must be an addition because it is a negative value
            self._t3chB.pulse_width_percent(100)
            
            
        
        pass
