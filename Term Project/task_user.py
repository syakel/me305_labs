'''@file                    task_user.py
   @brief                   Constructs the finite state machine for user_task
   @details                 The FSM allows the user to use commands to begin 
                            balancing the platform and stop balancing. 
                            
                            See state diagram below.
                            
                            \image html task_USER__states.png
                            
                           

   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    8 December 2021
'''

import pyb, utime
import time
import array as arr
import math
from micropython import const

## @brief   State 0, initialization
#
S0_INIT = const(0)

## @brief   State 1, waits for user input and responds to commands accordingly
#
S1_WAIT_FOR_INPUT = const(1)


class Task_User:
    ''' @brief              User task class
        @details            An object of this class is created in the main function file to
                            perform necessary tasks as outlined in the finite state machine.
    '''
    
    def __init__(self, period, balanceCMD, collectCMD):
        ''' @brief              Initializes and returns a user task object
            @details            Takes in required parameters from the main function file and
                                initializes all other required variables to run the state machine.
                                This includes initializing the serial port to take in user commands
                                from the keyboard, and setting the state to S0_INIT.
            @param period       Period for the user task (2 ms)
            @param balanceCMD   Shared object representing the user's command to begin balancing the ball
            @param collectCMD   Shared object representing the user's command to begin collecting data
        '''
        
        self._period = int(period)
        
        self._next_time = utime.ticks_add(utime.ticks_us(), self._period)
        
        self.state = 0
        
        self._serport = pyb.USB_VCP()
        
        self._balanceCMD = balanceCMD
        
        self._collectCMD = collectCMD
        
        self._command = S0_INIT
        
        ## @brief   State variable 
        #
        self.state = S0_INIT
        
        pass
    
    
    def run(self):
        '''@brief       Run the user task.
           @details     Uses a finite state machine to interact with the user.
                        Commands are entered through characters at the serial
                        port. See list_commands() for command options.
                        
        '''
        
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self._next_time) >= 0):
            
            self._next_time = current_time + self._period
            
            if self.state == 0:
                
                print("The available commands are:")
                print("    'b' or 'B': Balance ball")
                print("    's' or 'S': Stop balancing ball")
                print("    'c' or 'C': Collect data")
                print("    'e' or 'E': End data collection")
            
                self.transition_to(1)
            
            if self.state == 1:
                
            
                if self._serport.any():
                    self._command = self._serport.read(1)
        
                if self._command == b'b' or self._command == b'B':
                    self._balanceCMD.write(True)
                    print('Balancing Ball ...')
                    self._command = 0
                    self.transition_to(0)
                    
                elif self._command == b's' or self._command == b'S':
                    self._balanceCMD.write(False)
                    print('Stoping Balancing Ball')
                    self._command = 0
                    self.transition_to(0)
                    
                elif self._command == b'c' or self._command == b'C':
                    self._collectCMD.write(True)
                    print('Collecting data ...')
                    self._command = 0
                    self.transition_to(0)
                    
                elif self._command == b'e' or self._command == b'E':
                    self._collectCMD.write(False)
                    print('Ending data collection')
                    self._command = 0
                    self.transition_to(0)
                
                
                
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state
        