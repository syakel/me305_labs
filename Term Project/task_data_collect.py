''' @file                       task_data_collect.py
    @brief                      A task for printing collected data to the console.
    @details                    Uses a FSM to transition between initialization
                                and collection states
                                
                                See below for state diagram.
                                
                                \image html task_data_collect_states.png "Data Collection State Diagram"
    
    @author                     Seth Yakel
    @date                       8 December, 2021
''' 

import utime

## @brief   State 0, initialization
#
S0_INIT = 0

## @brief   State 1, collecting data
#
S1_COLLECT = 1

class Task_Data_Collect:
    ''' @brief                  A task for printing collected data to the console
        @details                This class is a task for reading the ball balancing 
                                platform data from shares and printing.

    '''
    
    def __init__(self, period, collectCMD, x, x_dot, y, y_dot, th_x, th_x_d, th_y, th_y_d):
        '''@brief       Creates a data collection task
           @details     This will instantiate a data collection object which 
                        collects data from the platform and print data to the console.                        
           @param       period          Period at which to print data.
           @param       collectCMD      Share indicating if the data collection
                                        should run
           @param       x               Share containing x position
           @param       x_dot           Share containing x_dot
           @param       y               Share containing y position
           @param       y_dot           Share containing y_dot
           @param       th_x            Share containing theta x
           @param       th_x_d          Share containing theta x dot
           @param       th_y            Share containing theta y
           @param       th_y_d          Share containing theta y dot
           
           
                       
        '''
        self._period = int(period)
        self._next_time = utime.ticks_add(utime.ticks_us(), self._period)
        
        self._x = x
        
        self._x_dot = x_dot
        
        self._y = y
        
        self._y_dot = y_dot
        
        self._th_x = th_x
        
        self._th_x_d = th_x_d
        
        self._th_y = th_x
        
        self._th_y_d = th_y_d
        
        self._collectCMD = collectCMD
        
        ## @brief   State variable 
        #
        self.state = 0



    def run(self):
        '''@brief       Run the data collect task.
           @details     Uses a FSM to transition between collection 
                        and initialization states using a shared command
                        variable set by the user.
        '''
        
        if (utime.ticks_us() >= self._next_time):
            
            self._next_time = self._next_time + self._period
            
            if (self.state == 0):
                
                if self._collectCMD.read() == True:
                    
                    self.transition_to(S1_COLLECT)
                
            
            elif(self.state == S1_COLLECT):
                
                x = self._x.read()
                xd = self._x_dot.read()
                y = self._y.read()
                yd = self._y_dot.read()
                thx = self._th_x.read()
                thxd = self._th_x_d.read()
                thy = self._th_y.read()
                thyd = self._th_y_d.read()
                
                currentTime = utime.ticks_us()
                
                print(f"{currentTime},,{x},{thy},{xd},{thyd},,{y},{thx},{yd},{thxd}")
                
                if self._collectCMD.read() == False:
                    
                    self.transition_to(0)
                    
                    
                    
                    
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state
                
                
                
                
                
                