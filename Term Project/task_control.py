'''@file                    task_control.py
   @brief                   Task for interaction with the motor controls
   @details                 A FSM which continuously updates the PWM for both
                            motors using the controller and motor drivers
                            
                            See below for state diagram.
                            
                            \image html task_control_states.png
                            
   
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    8 December 2021
'''
import utime
import controller_fsfb

## @brief   State 0, initialization
#
S0_INIT = 0

## @brief   State 1, updating
#
S1_UPDATE = 1

class Task_Control:
    ''' @brief          Control task class
        @details        An object of this class is created in the main function file to
                        perform necessary tasks as outlined in the finite state machine.
    '''
    
    def __init__(self, period, x, x_dot, y, y_dot, th_x, th_x_d, th_y, th_y_d, motorObj_1, motorObj_2, balanceCMD):
        ''' @brief                  Initializes controller task variables and objects
            @details                Takes in required parameters from the main function file and
                                    initializes all other required variables to run the state machine.
                                    This includes creating controller driver class objects,
                                    setting KT values, and setting the state of the FSM initially to S0_INIT.
            @param period           Period for the controller task (1 ms)
            @param x                Ball position (x)
            @param x_dot            Ball velocity (x)
            @param y                Ball position (y)
            @param y_dot            Ball velocity (y)
            @param th_x             Platform angle (x)
            @param th_x_d           Platform angular velocity (x)
            @param th_y             Platform angle (y)
            @param th_y_d           Platform angular velocity (y)
            @param motorObj_1       Motor object 1
            @param motorObj_2       Motor object 2
            @param balanceCMD       Shared object representing the user's command to begin balancing the ball

        '''
        
        self._period = int(period)
        
        self._balanceCMD = balanceCMD
        
        self._x = x
        
        self._x_dot = x_dot
        
        self._y = y
        
        self._y_dot = y_dot
        
        self._th_y = th_y
        
        self._th_y_d = th_y_d
        
        self._th_x = th_x
        
        self._th_x_d = th_x_d
        
        self._motor1 = motorObj_1
        
        self._motor2 = motorObj_2
        
        self._next_time = utime.ticks_add(utime.ticks_us(), self._period)
        
        ## @brief   State variable 
        #
        self.state = 0
        
        ## @brief   Controller driver object 1 (for first set of KT gains)
        #
        self.Control_1 = controller_fsfb.ClosedLoop()
        
                                #x       th_y       x_dot     th_y_dot
        self.Control_1.set_KT(0.00108, 0.48774, 0.00085, 0.014374)
        
        ## @brief   Controller driver object 2 (for second set of KT gains)
        #
        self.Control_2 = controller_fsfb.ClosedLoop()
        
                                #y       th_x       y_dot     th_x_dot
        self.Control_2.set_KT(-0.001300, -0.44774, -0.00095, -0.014374)
        
        
    def run(self): 
        '''@brief       Run the control task.
           @details     Uses a finite state machine to begin platform control
                        depending on commands from the user.
        '''
        
        if (utime.ticks_us() >= self._next_time):
            
            self._next_time = self._next_time + self._period
            
            if (self.state == S0_INIT):
                
                if self._balanceCMD.read() == True:
                    
                    self.transition_to(S1_UPDATE)
                
            
            elif(self.state == S1_UPDATE):
                
                # Duty cycle sent to T_x motor
                D_1 = self.Control_1.update(self._x.read(),self._th_y.read(), self._x_dot.read(), self._th_y_d.read())
                
                # Duty cycle sent to T_y motor
                D_2 = self.Control_2.update(self._y.read(), self._th_x.read(), self._y_dot.read(), self._th_x_d.read())
                
                #print('T_y Duty: ' + str(D_2))
               # if self._th_x.read() > -10:
                    
                    #D_2 = D_2*1.2
                    
                # if self._th_x.read() > -13:
                #     D_2 = D_2*1.3
                
                #if self._x.read() > 0:
                    #D_1 = -D_1
                    
                if self._y.read() > 0:
                    D_2 = 2*D_2
                    
                #if (self._y.read() < 5) & (self._y.read() > -5) & (self._y.read() != 0):
                 #   D_2 = 0
                    
                if D_1 > 70:
                    D_1 = 70
                    
                if D_1 < -70:
                    D_1 = -70
                    
                if D_2 > 70:
                    D_2 = 70
                    
                if D_2 < -70:
                    D_2 = -70
                    
                #if self._x.read() < 0:
                    #D_1 = -D_1
                    
                #if self._y.read() < 0:
                    #D_2 = -D_1
                
                self._motor1.set_duty(D_1)
                #print('Duty M_y')
                #print(D_1)
                
                self._motor2.set_duty(D_2)
                #print('Duty M_x')
                #print(D_2)
                
                
                if self._balanceCMD.read() == False:
                    
                    self.transition_to(S0_INIT)
                
                
                
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self.state = new_state