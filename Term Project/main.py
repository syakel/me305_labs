'''@file                    main.py
   @brief                   Main function file for the program
   @details                 
    
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    16 November 2021
'''

# Import necessary modules
import utime
import shares
import pyb
from pyb import I2C

# Import necessary task files
import task_touch
import task_IMU
import task_control
import task_user
import task_data_collect

# Import necessary driver files
import touch_panel
import BNO055
import motor_driver




def main():
    ''' @brief              Main function for the program
        @details            Instantiates the objects and tasks using imported
                            modules and performs multithreading of each 
                            necessary task.
    '''
    
    # Create share objects for ball position and velocity
    x = shares.Share(0)
    x_dot = shares.Share(0)
    y = shares.Share(0)
    y_dot = shares.Share(0)
    
    # Create share objects for platform angle and angular velocity
    theta_x = shares.Share(0)
    theta_x_dot = shares.Share(0)
    theta_y = shares.Share(0)
    theta_y_dot = shares.Share(0)
    
    # Create share objects for command variables
    balanceCMD = shares.Share(0)
    collectCMD = shares.Share(0)
    
    # Create pins for touch panel driver
    xm = pyb.Pin.cpu.A7
    xp = pyb.Pin.cpu.A1
    ym = pyb.Pin.cpu.A6
    yp = pyb.Pin.cpu.A0
    
    # Create pin objects for the first motor (T_y)
    B4 = pyb.Pin(pyb.Pin.cpu.B4)
    B5 = pyb.Pin(pyb.Pin.cpu.B5)
    
    # Create pin objects for the second motor (T_x)
    B0 = pyb.Pin(pyb.Pin.cpu.B0)
    B1 = pyb.Pin(pyb.Pin.cpu.B1)
    
    # Create motor driver object
    motor_drv       = motor_driver.Motor_Driver()
    
    # Create a motor object for motor 1 (T_y)
    motor_1         = motor_drv.motor(B4, B5, 1, 2)
    
    # Create a motor object for motor 2 (T_x)
    motor_2         = motor_drv.motor(B0, B1, 3, 4)
    
    # Create I2C object
    i2c = I2C(1, I2C.MASTER)
    
    # Define periods for each task
    period_0 = .02
    
    period_1 = .01
    
    period_2 = .001
    
    # Create a touch panel driver object
    touch_driver = touch_panel.Touch_Panel(xm, xp, ym, yp, 100, 176, 50, 88)
    
    # Create a BNO055 (IMU) driver object
    IMU_driver = BNO055.BNO055(i2c)
    
    # Create user task
    task_1 = task_user.Task_User(period_0, balanceCMD, collectCMD)
    
    # Create touch panel task
    task_2 = task_touch.Task_Touch(period_2, x, x_dot, y, y_dot, touch_driver)
    
    # Create IMU task
    task_3 = task_IMU.Task_IMU(period_0,  theta_x, theta_x_dot, theta_y, theta_y_dot, IMU_driver)
    
    # Create motor control task
    task_4 = task_control.Task_Control(period_2, x, x_dot, y, y_dot, theta_x, theta_x_dot, theta_y, theta_y_dot, motor_1, motor_2, balanceCMD)
    
    # Create data collection task
    task_5 = task_data_collect.Task_Data_Collect(period_1, collectCMD, x, x_dot, y, y_dot, theta_x, theta_x_dot, theta_y, theta_y_dot)
    
    
    
    
    ## A list of tasks to run (**)
    task_list = [task_1, task_2, task_3, task_4, task_5]



    while (True):
        try:
            for task in task_list:
                
                task.run()
            
        except KeyboardInterrupt:
            
            break

    print('Program Terminating')


if __name__=='__main__':
    
    main()