''' @file                   controller.py
    @brief                  Constructs a driver class for a general closed loop controller
    
    @details                ClosedLoop includes 3 functions: one to initialize the appropriate
                            variables, one to calculate the error and produce an acuation value,
                            and one to set the proportional gain Kp according to user input.
                            
    @author                 Seth Yakel
    @author                 Nicole Croft
    @date                   16 November 2021
'''

class ClosedLoop:
    
    ''' @brief              Defines the Closed Loop Class
        @details            Objects of this class can be used to perform closed
                            loop control. This class was written so that it 
                            can be used as a general controller.
    '''
    
    def __init__(self):
        ''' @brief          Completes setup and initializes the appropriate parameters
        '''
        
        
        pass
    
    def update(self, ref_value, meas_value):
        ''' @brief              Computes and returns the actuation value based on the measured and reference values
            @param ref_value    Reference value
            @param meas_value   Measured value
        '''
        self._error = ref_value - meas_value
        
        self._actuation = self._error * self._Kp 
        
        self._actuation = float(self._actuation)
        
        return self._actuation
        
        pass
    
    def set_Kp(self, value):
        ''' @brief          Sets the calculated Kp value
            @param value    Kp value
        '''
        self._Kp = value
        
        pass