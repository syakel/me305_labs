'''@file                    task_user.py
   @brief                   Constructs the finite state machine for user_task
   @details                 User_task includes three functions: one to initialize appropriate
                            variables, one to carry out appropriate actions within each state
                            and the proper movement between them, and one to
                            carry out the transition between each of the states. The final function
                            is not necessary, but makes the code more readable and user-friendly.
                            See description of the self.transition_to function for more details.
                            
                            The user task has 22 distinct states. See below for more details
                            
                            \image html task_user_states.png "User Task State Diagram"

   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    October 19, 2021
'''

import pyb, utime
import time
import array as arr
import math
from micropython import const

## @brief A variable representing state 0
#  @details This state is the initialization state
#
S0_INIT                 = const(0)

## @brief A variable representing state 1
#  @details This state prompts the user and waits for an input
#
S1_WAIT_FOR_INPUT       = const(1)

## @brief A variable representing state 2
#  @details This state triggers the flag variable and resets the enc_pos and delta to zero (ENC1)
#
S2_ZERO_POS             = const(2)

## @brief A variable representing state 3
#  @details This state reads and prints the most recent enc_pos value (ENC1)
#
S3_PRINT_POS            = const(3)

## @brief A variable representing state 4
#  @details This state reads and prints the most recent delta value (ENC1)
#
S4_PRINT_DELTA          = const(4)

## @brief A variable representing state 5
#  @details This state collects encoder position data for 30 seconds or until the user prompts termination with "s" (ENC1)
#
S5_COLLECT              = const(5)

## @brief A variable representing state 6
#  @details This state prints collected data if the user prompts termination preemptively (ENC1)
#
S6_END_COLLECT          = const(6)

## @brief A variable representing state 7
#  @details This state triggers the flag variable and resets the enc_pos and delta to zero (ENC2)
#
S7_ZERO_POS_ENC2        = const(7)

## @brief A variable representing state 8
#  @details This state reads and prints the most recent enc_pos value (ENC2)
#
S8_PRINT_POS_ENC2       = const(8)

## @brief A variable representing state 9
#  @details This state reads and prints the most recent delta value (ENC2)
#
S9_PRINT_DELTA_ENC2     = const(9)

## @brief A variable representing state 10
#  @details This state collects encoder position data for 30 seconds or until the user prompts termination with "s" (ENC2)
#
S10_COLLECT_ENC2        = const(10)

## @brief A variable representing state 11
#  @details This state prints collected data if the user prompts termination preemptively (ENC2)
#
S11_END_COLLECT_ENC2    = const(11)

## @brief A variable representing state 12
#  @details This state sets the duty cycle for motor 1
#
S12_SET_DUTY_CYCLE      = const(12)

## @brief A variable representing state 13
#  @details This state sets the duty cycle for motor 2
#
S13_SET_DUTY_CYCLE_ENC2      = const(13)

## @brief A variable representing state 14
#  @details This state takes proportional gain and velocity setpoint for motor 1 via user input
#
S14_STEP_RESPONSE_INPUT = const(14)

## @brief A variable representing state 15
#  @details This state collects data from motor 1
#
S15_STEP_RESPONSE_DATA_COLLECT     = const(15)

## @brief A variable representing state 16
#  @details This state ends collection for motor 1
#
S16_END_STEP_COLLECT               = const(16)

## @brief A variable representing state 17
#  @details This state takes proportional gain and velocity setpoint for motor 2 via user input
#
S17_STEP_RESPONSE_INPUT_2 = const(17)

## @brief A variable representing state 18
#  @details This state collects data from motor 2
#
S18_STEP_RESPONSE_DATA_COLLECT_2 = const(18)

## @brief A variable representing state 19
#  @details This state ends collection for motor 2
#
S19_END_STEP_COLLECT_2 = const(19)

## @brief A variable representing state 20
#  @details If a fault in motor 1 is triggered, this state clears it
#
S20_CLEAR_FAULT_1 = const(20)

## @brief A variable representing state 21
#  @details If a fault in motor 2 is triggered, this state clears it
#
S21_CLEAR_FAULT_2 = const(21)





class Task_User:
    ''' @brief              Defines the Task_User class
        @details            The class contains three functions: init, run, and transition_to.
                            The class defines appropriate variables and maps the prompts and
                            actions required by the task_user finite state machine.
    '''

    # Define the constructor
    def __init__(self, period, delta_1, enc_pos_1, flag_1, delta_2, enc_pos_2, flag_2, loop_mode, loop_mode_2, switch_1, switch_2, K_p, K_p_2, omega_ref_1, omega_ref_2, PWM_1, PWM_2, motor_1, motor_2):
        ''' @brief          Constructs an user task object
            @details        The init function initializes the state machine to the S0 INIT state,
                            initializes runs of the state machine to zero, and defines the serial
                            import so that the file can read user imports from the keyboard to
                            transition between states.
            @param          period Period at which the task will run
            @param          delta_1 Shared variable representing delta value for encoder 1
            @param          enc_pos_1 Shared variable representing position value for encoder 1
            @param          flag_1 Shared variable used as a trigger for zeroing encoder 1
            @param          delta_2 Shared variable representing delta value for encoder 2
            @param          enc_pos_2 Shared variable representing position value for encoder 2
            @param          flag_2 Shared variable used as a trigger for zeroing encoder 2
            @param          loop_mode Shared variable used for choosing motor 1 control modes
            @param          loop_mode_2 Shared variable used for choosing motor 2 control modes
            @param          switch_1 Shared variable used for triggering end of motor 1 control mode
            @param          switch_2 Shared variable used for triggering end of motor 2 control mode
            @param          K_p Shared variable used to set proportional gain for motor 1
            @param          K_p_2 Shared variable used to set proportional gain for motor 2
            @param          omega_ref_1 Shared variable used to set velocity setpoint for motor 1
            @param          omega_ref_2 Shared variable used to set velocity setpoint for motor 2
            @param          PWM_1 Shared variable used to set duty cycle for motor 1
            @param          PWM_2 Shared variable sued to set duty cycle for motor 2
            @param          motor_1 Object of one channel of the DRV8847 motor driver class
            @param          motor_2 Object of one channel of the DRV8847 motor driver class
        '''

        ## The period of the task (in ms)
        self.period = period

        ## A shares.Share object representing encoder position
        self.enc_pos_1 = enc_pos_1
        
        ## A shares.Share object representing encoder position
        self.enc_pos_2 = enc_pos_2
        
        ## A shares.Share object representing encoder position
        self.delta_1 = delta_1

        ## A shares.Share object representing change in encoder position
        self.delta_2 = delta_2

        ## The state to run in the finte state machine
        self.state = S0_INIT

        ## Number of runs of the finite state machine
        self.runs = 0

        ## Serial port for user input
        self.serport = pyb.USB_VCP()

        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        
        ## @brief A time variable
        #  @details Used to loop through the collection function
        #
        self.time_ = 0
        
        ## @brief Variable used in a time.sleep function
        #  @details Sets the amount of time which will be delayed
        #
        self.time_step = 1
        
        ## @brief A shared trigger variable used in between task_encoder and task_user
        #  @details Used to zero the position of the encoder by switching from low to high
        #
        self.flag_1 = flag_1
        
        ## @brief A shared trigger variable used in between task_encoder and task_user
        #  @details Used to zero the position of the encoder by switching from low to high
        #
        self.flag_2 = flag_2
        
        ## @brief A variable used for incremental counting
        #  @details Used to time the data collection in State 5
        #
        self.counter_ENC1 = 0
        
        ## @brief A variable used for incremental counting
        #  @details Used to time the data collection in State 10
        #
        self.counter_ENC2 = 0
        
        ## @brief A variable used for incremental counting
        #  @details Used to time the data collection in State 15
        #
        self.counter_Step_1 = 0
        
        ## @brief A variable used for incremental counting
        #  @details Used to time the data collection in State 18
        #
        self.counter_Step_2 = 0
        
        self._time_array = arr.array('f',1500*[0])
        
        self._pos_array = arr.array('f',1500*[0])
        
        self._omega_array = arr.array('f',1500*[0])

        self._omega_ref_1 = omega_ref_1
        
        self._omega_ref_2 = omega_ref_2
        
        ## @brief A shared trigger variable used in between task_controller and task_user
        #  @details Used to end control mode of the motor by switching from low to high
        #
        self.switch_1 = switch_1
        
        ## @brief A shared trigger variable used in between task_controller and task_user
        #  @details Used to end control mode of the motor by switching from low to high
        #
        self.switch_2 = switch_2
        
        ## @brief A shared variable used in task_controller and task_user
        #  @details Used to set the duty cycle for motor 1
        #
        self.PWM_1 = PWM_1
        
        ## @brief A shared variable used in task_controller and task_user
        #  @details Used to set the duty cycle for motor 2
        #
        self.PWM_2 = PWM_2
        
        ## @brief A shared variable used in task_controller and task_user
        #  @details Used to switch between motor 1 control states 
        #
        self.loop_mode_1 = loop_mode
        
        ## @brief A shared variable used in task_controller and task_user
        #  @details Used to switch between motor 2 control states 
        #
        self.loop_mode_2 = loop_mode_2
        
        self._step_time_array_1 = arr.array('f',501*[0])
        
        self._step_omega_array_1 = arr.array('f',501*[0])
        
        ## @brief A polymorphic object of the motor and DRV8847 driver classes
        #  @details Represents motor 1 and contains all the firmware necessary to control it
        #
        self.motor_1 = motor_1
        
        ## @brief A polymorphic object of the motor and DRV8847 driver classes
        #  @details Represents motor 2 and contains all the firmware necessary to control it
        #
        self.motor_2 = motor_2
        
        ## @brief A shared variable used in task_controller and task_user
        #  @details Used to set the proportional controller gain for motor 1
        #
        self.K_p = K_p
        
        ## @brief A shared variable used in task_controller and task_user
        #  @details Used to set the proportional controller gain for motor 2
        #
        self.K_p_2 = K_p_2
        
        
        
        

    def run(self):
        ''' @brief          Constructs the flow of the user_task finite state machine
            @details        The user_task finite state machine has 22 states: S0-S21.
                            This function runs each of these states and defines the proper
                            transitions between them according to the state diagram.
                            The serial import variable defined in the init function is utilized
                            so that user inputs can be interpreted and prompt the function to
                            transition between the states.

        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if (self.state==0):
                print(' ')
                print('Welcome. Initial position of the encoders is zero.')
                print('For all commands, lowercase is associated with motor and encoder 1,')
                print('and uppercase is associated with motor and encoder 2.')
                print(' ')
                print('Choose from list of input options: ')
                print('-To print the position of the encoder, press "p" or "P"')
                print('-To print delta between the last two positions, press "d" or "D"')
                print('-To re-zero the position of the encoder, press "z" or "Z"')
                print('-To collect position data for 30 seconds, press "g" or "G"')
                print('-To pre-emptively terminate data collection, press "s" or "S"')
                print('-To set a duty cycle for a motor, press "m" or "M"')
                print('-To perform a step response, press "v" or "V"')
                print('-To clear a fault triggered by the motor, press "c" or "C"')
                print(' ')
                print('-Waiting for user input: ')
            
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==1):
                if (self.serport.any()):
                    
                    ## @brief A variable used for user interaction 
                    #  @details Used to transition between states through serial input
                    #
                    self.prompt = self.serport.read(1)
                    if self.prompt == b'z':
                        self.transition_to(S2_ZERO_POS)
                    elif self.prompt == b'Z':
                        self.transition_to(S7_ZERO_POS_ENC2)
                    elif self.prompt == b'p':
                        self.transition_to(S3_PRINT_POS)
                    elif self.prompt == b'P':
                        self.transition_to(S8_PRINT_POS_ENC2)
                    elif self.prompt == b'd':
                        self.transition_to(S4_PRINT_DELTA)
                    elif self.prompt == b'D':
                        self.transition_to(S9_PRINT_DELTA_ENC2)
                    elif self.prompt == b'm':
                        self.transition_to(S12_SET_DUTY_CYCLE)
                    elif self.prompt == b'M':
                        self.transition_to(S13_SET_DUTY_CYCLE_ENC2)
                    elif self.prompt == b'g':
                        self.transition_to(S5_COLLECT)
                    elif self.prompt == b'G':
                        self.transition_to(S10_COLLECT_ENC2)
                    elif self.prompt == b'v':
                        self.transition_to(S14_STEP_RESPONSE_INPUT)
                    elif self.prompt == b'V':
                        self.transition_to(S17_STEP_RESPONSE_INPUT_2)
                    elif self.prompt == b'c':
                        self.transition_to(S20_CLEAR_FAULT_1)
                    elif self.prompt == b'C':
                        self.transition_to(S21_CLEAR_FAULT_2)
                        
                    else:
                        print('Command \'{:}\' is invalid.'.format(self.prompt))
                        print('Waiting for user input: ')

            elif (self.state==2):
                self.flag_1.write(1)
                print('Encoder 1 position zeroed.')
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==7):
                self.flag_2.write(1)
                print('Encoder 2 position zeroed.')
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==3):
                print('Encoder 1 position is: {:} radians'.format(self.enc_pos_1.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==8):
                print('Encoder 2 position is: {:} radians'.format(self.enc_pos_2.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==4):
                print('Encoder 1 Delta is: {:3f} rad/s'.format(self.delta_1.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==9):
                print('Encoder 2 Delta is: {:3f} rad/s'.format(self.delta_2.read()))
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==12):
                duty1 = input('Please enter a duty cycle for motor 1:')
                
                duty1 = float(duty1)
                
                self.PWM_1.write(duty1)
                
                self.loop_mode_1.write(1)
                
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==13):
                duty2 = input('Please enter a duty cycle for motor 2:')
                
                duty2 = float(duty2)
                
                self.PWM_2.write(duty2)
                
                self.loop_mode_2.write(1)
                
                self.transition_to(S1_WAIT_FOR_INPUT)

            elif (self.state==5):
                
                print('Collecting data points {:}/1500...'.format(self.counter_ENC1))
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b's':
                        self.transition_to(S6_END_COLLECT)
                    else:
                        pass
                
                if self.counter_ENC1 > 1499:
                    self.transition_to(S6_END_COLLECT)
                
                
                else:
                    self._time_array[self.counter_ENC1] = self.counter_ENC1*.02
                
                    self._pos_array[self.counter_ENC1] = self.enc_pos_1.read()*math.pi/2000
                
                    self._omega_array[self.counter_ENC1] = self.delta_1.read()
                    time.sleep(.02)
                
                    self.counter_ENC1 += 1
                
                
                
            elif (self.state==10):
                
                print('Collecting data points {:}/1500...'.format(self.counter_ENC2))
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b'S':
                        self.transition_to(S11_END_COLLECT_ENC2)
                    else:
                        pass
                
                if self.counter_ENC2 > 1499:
                    self.transition_to(S11_END_COLLECT_ENC2)
                
                else:
                    
                    self._time_array[self.counter_ENC2] = self.counter_ENC2*.02
                
                    self._pos_array[self.counter_ENC2] = self.enc_pos_2.read()*math.pi/2000
                
                    self._omega_array[self.counter_ENC2] = self.delta_2.read()
                
                    time.sleep(.02)
                
                    self.counter_ENC2 += 1
                
                        
            elif (self.state==6):
                print('Data collection ended.')
                
                for idx in range (0, self.counter_ENC1):
                    print('{:10.2f}, {:10.3f}, {:10.3f}'.format(self._time_array_1[idx],self._pos_array_1[idx],self._omega_array_1[idx]))
                
                self.counter_ENC1 = 0
                print('Waiting for user input: ')    
                self.transition_to(S1_WAIT_FOR_INPUT)


            elif (self.state==11):
                print('Data collection ended.')
                
                for idx in range (0, self.counter_ENC2):
                    print('{:10.2f}, {:10.3f}, {:10.3f}'.format(self._time_array_2[idx],self._pos_array_2[idx],self._omega_array_2[idx]))
                
                self.counter_ENC2 = 0
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            elif (self.state==14):
                
                K_p = input ('Please enter a proportional gain for motor 1:')
                
                K_p = float(K_p)
                
                velo = input('Please enter a velocity setpoint for motor 1:')
                
                velo = float(velo)
                
                self.K_p.write(K_p)
                
                self.omega_ref_1.write(velo)
                
                
                self.transition_to(S15_STEP_RESPONSE_DATA_COLLECT)
                
                self.loop_mode_1.write(2)
                
                
            elif (self.state==15):
                
                print('Collecting data points {:}/500...'.format(self.counter_Step_1))
                
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b's':
                        self.switch_1.write(1)
                        self.transition_to(S16_END_STEP_COLLECT)
                    else:
                        pass
                
                if self.counter_Step_1 > 500:
                    self.switch_1.write(1)
                    self.transition_to(S16_END_STEP_COLLECT)
                    
                else:
                    

                    self._step_time_array_1[self.counter_Step_1] = self.counter_Step_1*.02
                
                    self._step_omega_array_1[self.counter_Step_1] = (self.delta_1.read())
                
                    time.sleep(.02)
                
                    self.counter_Step_1 += 1
                
            elif (self.state==16):
                
                print('Data collection ended.')
                
                for idx in range (0, self.counter_Step_1):
                    print('{:10.2f}, {:10.3f}'.format(self._step_time_array_1[idx],self._step_omega_array_1[idx]))
                
                self.counter_Step_1 = 0
                
                
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
                
            elif (self.state==17):
                
                K_p = input ('Please enter a proportional gain for motor 2:')
                
                K_p = float(K_p)
                
                velo = input('Please enter a velocity setpoint for motor 2:')
                
                velo = float(velo)
                
                self.K_p_2.write(K_p)
                
                self.omega_ref_2.write(velo)
                
                
                self.transition_to(S18_STEP_RESPONSE_DATA_COLLECT_2)
                
                self.loop_mode_2.write(2)
            
            elif (self.state==18):
                
                print('Collecting data points {:}/500...'.format(self.counter_Step_2))
                
                if (self.serport.any()):
                    self.prompt = self.serport.read(1)
                    if self.prompt == b'S':
                        self.switch_2.write(1)
                        self.transition_to(S19_END_STEP_COLLECT_2)
                    else:
                        pass
                
                if self.counter_Step_2 > 500:
                    self.switch_2.write(1)
                    self.transition_to(S19_END_STEP_COLLECT_2)
                    
                else:
                
                    self._step_time_array_1[self.counter_Step_2] = self.counter_Step_2*.02
                
                    self._step_omega_array_1[self.counter_Step_2] = self.delta_2.read()
                
                    time.sleep(.02)
                
                    self.counter_Step_2 += 1
                    
            elif (self.state==19):
                
                print('Data collection ended.')
                
                for idx in range (0, self.counter_Step_2):
                    print('{:10.2f}, {:10.3f}'.format(self._step_time_array_1[idx],self._step_omega_array_1[idx]))
                
                self.counter_Step_2 = 0
                
                
                print('Waiting for user input: ')
                self.transition_to(S1_WAIT_FOR_INPUT)
                
                
                
            elif (self.state==20):
                
                self.motor_1.enable()
                
                self.transition_to(S1_WAIT_FOR_INPUT)
                
            else:
                raise ValueError('Invalid State')

            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1

    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==1",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(1)", the code displays
                            "self.transition_to(S1_WAIT_FOR_INPUT)".
            @param new_state    An integer value indicating the next state
        '''
        self.state = new_state


