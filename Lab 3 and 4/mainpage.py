'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py 

   @mainpage

   @section sec_intro   Introduction
                        This portfolio documents the process which we took to
                        design a balancing platform for the class ME 305.
                        
                        The source code for this project can be found at:
                            
                        https://bitbucket.org/syakel/me305_labs/src/master/
                        
                        TuningReport.html
   

   @author              Seth Yakel

   @date                November 16, 2021
   
'''