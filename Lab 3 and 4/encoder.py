'''@file                    encoder.py
   @brief                   Constructs the encoder driver class
   @details                 The class includes five functions: init, update,
                            get_position, set_position, and get_delta. Position and delta values
                            are obtained using these functions, and the information is shared
                            to the task_encoder class by utilizing a "shares" class. See
                            details for each function within the class for more information.
   @author                  Seth Yakel
   @author                  Nicole Croft
   @date                    16 November 2021
'''
import time
import utime
import pyb
import math

class Encoder:
    ''' @brief              Constructs an encoder class
        @details            The class contains five functions: init, update, get_position,
                            set_position, and get_delta. See details for each function within
                            the class for more information.
    '''

    def __init__(self, tim, pinA, pinB):
        ''' @brief          Constructs an encoder object
            @details        This function assigns and initializes proper variables in order
                            for position and delta information to be calculated and updated.
                            This includes assigning proper pins and timers associated with
                            the encoder, and initializing the encoder position and delta values
                            to zero.
            @param          tim Timer channel object
            @param          pinA CPU Pin object
            @param          pinB CPU Pin object
        '''
        
        ## @brief Variable representing PinB6
        #  @details Instantiated object of PinB6
        #
        self.pinB = pinA
        
        ## @brief Variable representing PinB7
        #  @details Instantiated object of PinB7
        #
        self.pinB = pinB
        
        ## @brief Variable representing tim4 object
        #  @details Instantiated object of tim4
        #
        self.tim = tim
        
        ## @brief Variable representing the channel object
        #  @details Instantiated channel object
        #
        self.timch1 = tim.channel(1, tim.ENC_A, pin=pinA)
        
        ## @brief Variable representing the channel object
        #  @details Instantiated channel object
        #
        self.timch2 = tim.channel(2, tim.ENC_AB, pin=pinB)
        
        ## @brief Variable used in the update function
        #  @details Used to calculate the delta in update method
        #
        self.pos1 = 0
        
        ## @brief Variable used to represent the current encoder position
        #  @details Is set with the update method
        #
        self.enc_pos = 0
        
        ## @brief Variable representing the difference between the current and last encoder position
        #  @details Used in the update method
        #
        self.delta = 0
        
        self._last_count = tim.counter()
        
        self._next_count = tim.counter()
        
        self._last_time = utime.ticks_us()
        
        self._next_time = utime.ticks_us()
        
        
        ## @brief Variable used to zero the encoder position
        #  @details Used as a value to offset the encoder position back to zero
        #
        


    def update(self):
        ''' @brief          Updates encoder position and delta
            @details        This function calculates the position and delta 
                            values for the encoder with respect to time. This
                            will account for underflows and overflows
                            when the encoder ticks drop below 0 and surpass 65,535 
                            tick, allowing for accurate values beyond the resolution
                            of the encoder. The delta value is calculated using a 
                            time and position difference between function runs. The
                            use of the utime module allows for more accurate time 
                            differences, because our task does not always run 
                            perfectly on time.
                        
        '''
        self._next_time = utime.ticks_us()
        
        self._next_count = self.tim.counter()

        self.delta = self._next_count - self._last_count
        
        self._t_diff = (utime.ticks_diff(self._next_time, self._last_time))/1000000
        
        self._last_count = self._next_count
        
        self._last_time = self._next_time
                
        if self.delta > 32767.5:
                    self.delta = self.delta - 65535
                
        elif self.delta < -32767.5:
                    self.delta = self.delta + 65535
                    

        ##new position of the encoder
        #
        self.enc_pos += self.delta

        ##change in position since last tracked [rad/s]
        #
        
        self.delta_rad = (self.delta/self._t_diff)*2*math.pi/4000


    def get_position(self):
        ''' @brief          Returns encoder position
            @details        This function does not perform any calculations- it only
                            returns the current encoder position. This function is
                            utilized by the share class for proper communication of
                            information within the program
            @return         Current encoder position
        '''
        return self.enc_pos

    def set_position(self):
        ''' @brief          Sets encoder position
            @details        This function is utilized when the user wishes to reset the
                            encoder position to zero. Encoder position and delta values
                            are reset to zero.
            @return         Returns zero value for encoder position
        '''
        self.enc_pos = 0
        self.delta = 0
        self.delta_rad = 0
        
        
        return 0
        #resets the position to a specified value (I'm assuming we'll want this to be zero).
        #make sure variable changes are accounted for so that update() still works
        #print('Setting position and delta values')

    def get_delta(self):
        ''' @brief          Returns encoder delta
            @details        This function does not perform any calculations- it only
                            returns the current change in encoder position. This function is
                            utilized by the share class for proper communication of
                            information within the program
            @return         Current delta value (change in position)/(change in time)
        '''
        return self.delta_rad