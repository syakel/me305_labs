
''' @file                   task_controller.py
    @brief                  Task file for closed loop speed control
    
    @details                This is a separate task file incorporating closed loop speed control.
                            Task is integrated into the existing task structure.
    @author                 Seth Yakel
    @author                 Nicole Croft
    @date                   16 November 2021
'''


import controller
import DRV8847
import shares
import utime
from micropython import const
import math


_S0_INIT = const(0)

_S1_WAIT_FOR_INPUT = const(1)

_S2_OPEN_LOOP = const(2)

_S3_CLOSED_LOOP = const(3)


class Task_Controller:
    ''' @brief              Defines the Controller Task Class
        @details            Objects of this class are initialized and then can
                            be controlled using both closed loop and open loop
                            control.
    '''
    
    def __init__(self, _period, _K_p, _omega_ref, _omega_meas, _loop_mode, _ControllerDriverObj, _switch, _PWM, _motorObject, _delta):
        ''' @brief          Completes setup and initializes the appropriate parameters
            @details        
        '''
        
        self._state = _S0_INIT
        
        self._period = _period
        
        self._K_p = _K_p
        
        self._omega_ref = _omega_ref
        
        self._omega_meas = _omega_meas
        
        self._loop_mode = _loop_mode
        
        self._switch = _switch
        
        self._Controller = _ControllerDriverObj
        
        self._Motor = _motorObject
        
        self._delta = _delta
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self._next_time = utime.ticks_add(utime.ticks_us(), self._period)
        
        self._PWM = _PWM
        
        
    def run(self):
        ''' @brief      Runs the controller task
            @details    This method runs as a finite state machine containing 4
                        states: an initialization state, an idle wait for input state
                        an open loop control state, and a closed loop control state. 
                        The transition from state to state is determined by the user 
                        input from the user task through the use of a share object named loop_mode.
                        
        '''
        
        if (utime.ticks_us() >= self._next_time):
            
            if (self._state == _S0_INIT):
                
                self.transition_to(_S1_WAIT_FOR_INPUT)
                
            elif (self._state == _S1_WAIT_FOR_INPUT):
                
                self._switch.write(0) 
                
                if (self._loop_mode.read() == 1):
                    
                    self.transition_to(_S2_OPEN_LOOP)
                    
                if (self._loop_mode.read() == 2):
                    
                    self._Controller.set_Kp(self._K_p.read())
                    
                    self.transition_to(_S3_CLOSED_LOOP)
                    
            elif (self._state == _S2_OPEN_LOOP):
                
                self._loop_mode.write(0)
                
                if (self._switch.read() == 1):
                    self.transition_to(_S1_WAIT_FOR_INPUT)
                    
                else:
                    self._Motor.set_duty(self._PWM.read())
                    
                    
                    self.transition_to(_S1_WAIT_FOR_INPUT)
                
            elif (self._state == _S3_CLOSED_LOOP):
                
                self._loop_mode.write(0)
                
                if (self._switch.read() == 1):
                    self.transition_to(_S1_WAIT_FOR_INPUT)
                    
                else:
                    #print('Controller updated')
                    
                    _omega_meas = float(self._delta.read()*2*math.pi/4000)
                    
                    self._PWM.write(self._Controller.update(float(self._omega_ref.read()), _omega_meas))
        
                    self._Motor.set_duty(self._PWM.read())
        
        
    
    def transition_to(self, new_state):
        ''' @brief          Transitions the machine from one state to the next
            @details        This function allows for better readability of the code.
                            Rather than prompting the next state with "self.state==0",
                            This function allows the code to be "self.transition_to(__)".
                            State values were initialized with names so that each state's
                            function is indicated in the code as well. Rather than
                            "self.transition_to(0)", the code displays
                            "self.transition_to(S0_INIT)".
        '''
        self._state = new_state
    
    
    
    
    
    
    
    
    
    
    