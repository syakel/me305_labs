# -*- coding: utf-8 -*-
# @file fibonacci.py

"""
Created on Tue Sep 21 12:42:43 2021

@author: Seth Yakel
"""
def fib(int):
    # This function calculates a Fibonacci number at a specific index
    # int represents the index input by the user specifying the desired 
    # fibonacci number
    
    # This funtion calculates the fibonacci number by creating an empty array 
    # equal to the length of the desired index and calculating each individual
    # fibonacci number up until that index. This is the "bottom up method".
    array = [0]*(int+1)
    
    array[0] = 0
    array[1] = 1
    
    i = 2
    
    while i<=len(array)-1:
        
        array[i] = array[i-1] + array[i-2]
        
        i += 1
        
    fib_index = array[int]
    return fib_index



def my_fib():    
    
    # This function prompts the user for their desired Fibonacci index, and
    # returns to them that value by calling the function fib().
    
    index = input('Please enter an index: ')
    index = int(index)
    
    
    if index < 0 :
        print('Not a Valid Index, please enter an index greater than or equal to 0')   
        my_fib()
        # This function contains logic which will reject invalid index requests
        # from the user, and will prompt them to reenter a valid one.
    else:
       answer = fib(index)
       
       print('Fibonacci number at '
             'index {:} is {:}.'.format(index,answer))
       
       prompt = input('Enter q to quit or enter to continue: ')
       
       if prompt in ['q', 'Q', 'quit']:
           pass
       # Upon returning the fibonacci number to the user, the function will
       # prompt the user to continue the program or quit.
       else:
           my_fib()
    
if __name__ == '__main__':
    my_fib()
    
    
